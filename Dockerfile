FROM alpine/git as clone
WORKDIR /app
RUN git clone https://gricad-gitlab.univ-grenoble-alpes.fr/soretl/devops_project_2019.git

FROM maven:3.5-jdk-8-alpine as build
WORKDIR /app
COPY --from=clone /app/devops_project_2019 /app
RUN mvn install

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=build /app/target/DevOps_Project_2019-1.0-SNAPSHOT-javadoc.jar /app
COPY --from=build /app/target/DevOps_Project_2019-1.0-SNAPSHOT-jar-with-dependencies.jar /app/DevOps_Project_2019-1.0-SNAPSHOT.jar
ENTRYPOINT ["sh", "-c"]
CMD ["java -jar DevOps_Project_2019-1.0-SNAPSHOT.jar"]