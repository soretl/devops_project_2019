import Exceptions.WrongOperationOnColumnException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class ColumnStringTest {
    private ColumnString column;
    ArrayList<String> columnModel;

    @BeforeEach
    public void beforeEach(){
        columnModel = new ArrayList<>();
        columnModel.add("Jonathan");
        columnModel.add("Joseph");
        columnModel.add("Jotaro");
        columnModel.add("Josuke");
        columnModel.add("Giorno");
        column = new ColumnString("Nom", columnModel);
    }

    @Test
    public void Constructor_WithArrayList_ContainsSameStrings(){
        assertEquals("Jonathan", column.get(0));
        assertEquals("Joseph", column.get(1));
        assertEquals("Jotaro", column.get(2));
        assertEquals("Josuke", column.get(3));
        assertEquals("Giorno", column.get(4));
    }

    @Test
    public void Constructor_OnlyWithLabel_SizeEqualsZero(){
        Column emptyColumn = new ColumnNumber("label");
        assertEquals(0, emptyColumn.size());
    }

    @Test
    public void Constructor_OnlyWithLabel_HasCorrectLabel(){
        Column emptyColumn = new ColumnNumber("label");
        assertEquals("label", emptyColumn.getLabel());
    }

    @Test
    public void get_GetCorrectIndex_HasCorrectValue(){
        assertEquals("Joseph", column.get(1));
    }

    @Test
    public void remove_RemoveCorrectIndex_HasDifferentSize(){
        column.remove(1);
        assertEquals(4, column.size());
    }

    @Test
    public void removeDuplicates_WithNoDuplicates_ColumnIsUnchanged(){
        ColumnString column2 = column;
        column2.removeDuplicates();
        assertEquals(column, column2);
    }

    @Test
    public void removeDuplicates_WithDuplicates_DuplicatesAreRemoved(){
        ArrayList<String> namesWithNoDuplicates = new ArrayList<>();
        namesWithNoDuplicates.add("Jonathan");
        namesWithNoDuplicates.add("Joseph");
        Column columnWithNoDuplicates = new ColumnString("Noms", namesWithNoDuplicates);

        ArrayList<String> namesWithDuplicates = new ArrayList<>();
        namesWithDuplicates.add("Jonathan");
        namesWithDuplicates.add("Joseph");
        namesWithDuplicates.add("Jonathan");
        Column columnWithDuplicates = new ColumnString("Noms", namesWithDuplicates);
        columnWithDuplicates.removeDuplicates();

        assertEquals(columnWithNoDuplicates, columnWithDuplicates);
    }

    @Test
    public void clear_OnColumn_ShouldBeEmpty(){
        column.clear();
        assertTrue(column.size() == 0);
    }

    @Test
    public void getLabel_NoArguments_ReturnsCorrectLabel(){
        assertEquals("Nom", column.getLabel());
    }

    @Test
    public void size_NoArguments_ReturnsCorrectSize(){
        assertEquals(5, column.size());
    }

    @Test
    public void add_ElementOfSameType_ElementAddedToTheColumn(){
        String nom = "Jolyne";
        column.add(nom);
        assertEquals(nom, column.get(column.size()-1));
    }

    @Test
    public void sum_OnColumn_ThrowsException(){
        assertThrows(WrongOperationOnColumnException.class, () -> column.sum());
    }

    @Test
    public void average_OnColumn_ThrowsException(){
        assertThrows(WrongOperationOnColumnException.class, () -> column.average());
    }

    @Test
    public void minimum_OnColumn_ThrowsException(){
        assertThrows(WrongOperationOnColumnException.class, () -> column.minimum());
    }

    @Test
    public void maximum_OnColumn_ThrowsException(){
        assertThrows(WrongOperationOnColumnException.class, () -> column.maximum());
    }

    @Test
    public void testEquals_Symmetric(){
        ColumnString x = new ColumnString("test", columnModel);  // equals and hashCode check name field value
        ColumnString y = new ColumnString("test", columnModel);
        assertTrue(x.equals(y) && y.equals(x));
        assertTrue(x.hashCode() == y.hashCode());
    }
}
