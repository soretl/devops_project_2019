import Exceptions.ColumnHaveDifferentTypesException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DataframeTest {
    private Dataframe dataframe;
    private ArrayList<Column> dataframeModel;
    private Dataframe emptyDataframe;
    private Dataframe zooDataframe;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void beforeEach() throws ColumnHaveDifferentTypesException {
        // the personnes.csv in an array list
        dataframeModel = new ArrayList<>();
        ColumnString nameColumn = new ColumnString("Nom");
        nameColumn.add("Jonathan");
        nameColumn.add("Giorno");
        nameColumn.add("Joseph");
        nameColumn.add("Gappy");

        ColumnNumber ageColumn = new ColumnNumber("Âge");
        long[] numbers = new long[]{20, 15, 91, 19};
        ageColumn.add(numbers[0]);
        ageColumn.add(numbers[1]);
        ageColumn.add(numbers[2]);
        ageColumn.add(numbers[3]);

        dataframeModel.add(nameColumn);
        dataframeModel.add(ageColumn);

        // the dataframe to test with csv
        dataframe = new Dataframe("personnes.csv");

        // An other dataframe to test with csv
        zooDataframe = new Dataframe("zoo.csv");

        // An empty dataframe to test on
        emptyDataframe = new Dataframe("empty.csv");

        // To test print methods
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void afterEach(){
        System.setOut(originalOut);
    }

    @Test
    public void Constructor_WithArrayList_ShouldBeUnchanged(){
        Dataframe newDataframe = new Dataframe(dataframeModel);

        assertEquals(dataframeModel.get(0), newDataframe.get(0));
        assertEquals(dataframeModel.get(1), newDataframe.get(1));
    }

    @Test
    public void Constructor_WithCSVFile_ShouldBeSameThanOriginalFile() {
        Dataframe newDataframeWithArrayList = new Dataframe(dataframeModel);

        assertEquals(dataframe, newDataframeWithArrayList);
    }

    @Test
    public void Constructor_WithEmptyCSVFile_ShouldHaveSizeEqualToZero(){
        assertEquals(0, emptyDataframe.size());
    }

    @Test
    public void Constructor_WithCSVWithOnlyLabels_ShouldHaveSizeEqualsZero() throws ColumnHaveDifferentTypesException {
        Dataframe newDataframe = new Dataframe("onlyLabels.csv");
        assertEquals(0, newDataframe.size());
    }

    @Test
    public void Constructor_WithCSVWithTypeError_ShouldThrowException(){
        assertThrows(ColumnHaveDifferentTypesException.class, () -> new Dataframe("differentTypes.csv"));
    }

    @Test
    public void get_OnColumnThatExists_ShouldReturnExpectedValue(){
        Column column = dataframe.get(1);
        assertEquals("Âge", column.getLabel());
    }

    @Test
    public void get_OnColumnThatDoesntExists_ShouldThrowException(){
        assertThrows(IndexOutOfBoundsException.class, () -> dataframe.get(42));
    }

    @Test
    public void get_OnEmptyDataframe_ShouldThrowException(){
        Dataframe newDataframe = new Dataframe(new ArrayList<Column>());
        assertThrows(IndexOutOfBoundsException.class, () -> newDataframe.get(0));
    }

    @Test
    public void size_OnTestArray_ShouldHaveSameSize(){
        assertEquals(2, dataframe.size());
    }

    @Test
    public void isEmpty_OnNonEmptyDataframe_ShouldReturnTrue(){
        assertFalse(dataframe.isEmpty());
    }

    @Test
    public void isEmpty_OnEmptyDataframe_ShouldReturnTrue(){
        assertTrue(emptyDataframe.isEmpty());
    }

    @Test
    public void getHeight_OnEmptyArray_ShouldReturnZero() {
        assertEquals(0, emptyDataframe.getHeight());
    }

    @Test
    public void getHeight_OnDataframe_ShouldReturnExpectedWidth() throws ColumnHaveDifferentTypesException {
        assertEquals(4, dataframe.getHeight());
    }

    @Test
    public void toString_ReturnExpectedString(){
        String expectedString = "Dataframe{columns=[Column{label='Nom', content=[Jonathan, Giorno, Joseph, Gappy]}, Column{label='Âge', content=[20, 15, 91, 19]}]}";
        assertEquals(expectedString, dataframe.toString());
    }

    @Test
    public void print_WithDatafraframe_ShouldPrintContentOfDataframe(){
        String expectedString = "[Nom][Âge]\n[Jonathan][20]\n[Giorno][15]\n[Joseph][91]\n[Gappy][19]\n";

        dataframe.print();
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void print_EmptyDataframe_ShouldPrintEmptyString() {
        String expectedString = "\n";
        emptyDataframe.print();
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printLine_ExistingLine_ShouldPrintLine(){
        String expectedString = "[Joseph][91]\n";
        dataframe.printLine(2);
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printLine_ExistingLine_ShouldReturnOne(){
        int error = dataframe.printLine(2);
        assertEquals(1, error);
    }

    @Test
    public void printLine_WithNegativeLine_ShouldReturnZero(){
        int error = dataframe.printLine(-1);
        assertEquals(0, error);
    }

    @Test
    public void printLine_LineThatDoesNotExist_ShouldReturnZero(){
        int error = dataframe.printLine(42);
        assertEquals(0, error);
    }

    @Test
    public void printLine_LineThatDoesNotExist_ShouldPrintEmptyString(){
        String expectedString = "";
        dataframe.printLine(42);
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printLabels_ExistingLabels_ShouldPrintLabels(){
        String expectedString = "[Nom][Âge]\n";
        dataframe.printLabels();
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printLabels_WithEmptyDataframe_ShouldPrintEmptyString(){
        String expectedString = "\n";
        emptyDataframe.printLabels();
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printFirstLines_OnExistingLines_ShouldPrintLines(){
        String expectedString = "[Nom][Âge]\n[Jonathan][20]\n[Giorno][15]\n";

        dataframe.printFirstLines(2);
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printFirstLines_OnNonTooManyLines_ShouldPrintOnlyExistingLines(){
        String expectedString = "[Nom][Âge]\n[Jonathan][20]\n[Giorno][15]\n[Joseph][91]\n[Gappy][19]\n";

        dataframe.printFirstLines(10);
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printFirstLines_OnNegativeAmountOfLines_ShouldPrintOnlyLabels(){
        String expectedString = "[Nom][Âge]\n";

        dataframe.printFirstLines(-1);
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printFirstLines_OnNullAmountOfLines_ShouldPrintOnlyLabels(){
        String expectedString = "[Nom][Âge]\n";

        dataframe.printFirstLines(0);
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printLastLines_OnExistingLines_ShouldPrintLines(){
        String expectedString = "[Nom][Âge]\n[Joseph][91]\n[Gappy][19]\n";

        dataframe.printLastLines(2);
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printLastLines_OnNonTooManyLines_ShouldPrintOnlyExistingLines(){
        String expectedString = "[Nom][Âge]\n[Jonathan][20]\n[Giorno][15]\n[Joseph][91]\n[Gappy][19]\n";

        dataframe.printLastLines(10);
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printLastLines_OnNegativeAmountOfLines_ShouldPrintOnlyLabels(){
        String expectedString = "[Nom][Âge]\n";

        dataframe.printLastLines(-1);
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void printLastLines_OnNullAmountOfLines_ShouldPrintOnlyLabels(){
        String expectedString = "[Nom][Âge]\n";

        dataframe.printLastLines(0);
        assertEquals(expectedString, outContent.toString());
    }

    @Test
    public void iloc_WithIndex_ShouldHaveSameLabels(){
        Dataframe selectedDataframe = dataframe.iloc(1);
        for(int i = 0; i < dataframe.size(); i++){
            assertEquals(dataframe.get(i).getLabel(), selectedDataframe.get(i).getLabel());
        }
    }

    @Test
    public void iloc_WithIndex_ShouldCopyTheLine(){
        Dataframe selectedDataframe = dataframe.iloc(2);
        for(int i = 0; i < dataframe.size(); i++){
            assertEquals(dataframe.get(i).get(2), selectedDataframe.get(i).get(0));
        }
    }

    @Test
    public void iloc_WithIndexTooBig_ShouldBeEmpty(){
        Dataframe selectedDataframe = dataframe.iloc(4);
        assertTrue(selectedDataframe.isEmpty());
    }

    @Test
    public void iloc_WithIndexTooSmall_ShouldBeEmpty(){
        Dataframe selectedDataframe = dataframe.iloc(-1);
        assertTrue(selectedDataframe.isEmpty());
    }

    @Test
    public void iloc_WithIndicesArray_ShouldHaveSameLabel(){
        int[] indices = new int[]{0, 1, 2, 3};
        Dataframe selectedDataframe = dataframe.iloc(indices);
        for(int i = 0; i < dataframe.size(); i++){
            assertEquals(dataframe.get(i).getLabel(), selectedDataframe.get(i).getLabel());
        }
    }

    @Test
    public void iloc_WithIndicesArray_ShouldReturnSameDataframe(){
        int[] indices = new int[]{0, 1, 2, 3};
        Dataframe selectedDataframe = dataframe.iloc(indices);
        assertEquals(dataframe, selectedDataframe);
    }

    @Test
    public void iloc_WithNonExistingIndicesArray_ShouldBeEmpty(){
        int[] indices = new int[]{42, -1};
        Dataframe selectedDataframe = dataframe.iloc(indices);

        assertTrue(selectedDataframe.isEmpty());
    }

    @Test
    public void iloc_WithCorrectRange_ShouldReturnSameDataframe(){
        Dataframe selectedDataframe = dataframe.iloc(0, 3);
        assertEquals(dataframe, selectedDataframe);
    }

    @Test
    public void iloc_WithRangeAndStartBiggerThanStop_ShouldReturnEmptyDataframe(){
        Dataframe selectedDataframe = dataframe.iloc(10, 5);
        assertTrue(selectedDataframe.isEmpty());
    }

    @Test
    public void iloc_WithRangeAndNegativeStart_ShouldReturnSameDataframe(){
        Dataframe selectedDataframe = dataframe.iloc(-10, 2);
        assertTrue(selectedDataframe.isEmpty());
    }

    @Test
    public void iloc_WithBiggerRangeThanDataframe_ShouldReturnSameDataframe(){
        Dataframe selectedDataframe = dataframe.iloc(0, 10);

        assertEquals(dataframe, selectedDataframe);
    }

    @Test
    public void loc_WithLabel_ShouldCopyTheColumn(){
        Dataframe selectedDataframe = dataframe.loc("Nom");

        assertEquals(dataframe.get(0), selectedDataframe.get(0));
    }

    @Test
    public void loc_WithWrongLabel_ShouldBeEmpty(){
        Dataframe selectedDataframe = dataframe.loc("blblblbl");
        assertTrue(selectedDataframe.isEmpty());
    }

    @Test void loc_WithArrayOfLabels_ReturnSameDataframe(){
        Dataframe selectedDataframe = dataframe.loc("Nom", "Âge");
        assertEquals(dataframe, selectedDataframe);
    }

    @Test void loc_WithArrayOfLabelsAndStopIsAfterStart_ReturnEmptyDataframe(){
        Dataframe selectedDataframe = dataframe.loc("Âge", "Nom");
        assertTrue(selectedDataframe.isEmpty());
    }

    @Test void loc_WithLabelsThatDoesNotExist_ReturnEmptyDataframe(){
        Dataframe selectedDataframe = dataframe.loc("1", "2");
        assertTrue(selectedDataframe.isEmpty());
    }

    @Test
    public void groupBy_OnZooCSV_DoesNotChangeOriginalDataframe() throws ColumnHaveDifferentTypesException {
        Dataframe original = new Dataframe("zoo.csv");
        zooDataframe.groupBy("Espèce", Function.AVERAGE);
        assertEquals(original, zooDataframe);
    }

    @Test
    public void groupBy_OnZooCSVWithAverageOnSpecies_ShouldHaveExpectedModel(){
        ArrayList<Column> zooModel = new ArrayList<>();

        ColumnString species = new ColumnString("Espèce");
        species.add("Ours");
        species.add("Chien");
        species.add("Oiseau");
        species.add("Porc");
        zooModel.add(species);

        ColumnNumber ages = new ColumnNumber("Âge");
        ages.add(13.0);
        ages.add(6.5);
        ages.add(9.333333333333334);
        ages.add(5.0);
        zooModel.add(ages);

        assertEquals(new Dataframe(zooModel), zooDataframe.groupBy("Espèce", Function.AVERAGE));
    }

    @Test
    public void groupBy_OnZooCSVWithSumOnSpecies_ShouldHaveExpectedModel(){
        ArrayList<Column> zooModel = new ArrayList<>();

        ColumnString species = new ColumnString("Espèce");
        species.add("Ours");
        species.add("Chien");
        species.add("Oiseau");
        species.add("Porc");
        zooModel.add(species);

        ColumnNumber ages = new ColumnNumber("Âge");
        ages.add(26.0);
        ages.add(13.0);
        ages.add(28.0);
        ages.add(5.0);
        zooModel.add(ages);

        assertEquals(new Dataframe(zooModel), zooDataframe.groupBy("Espèce", Function.SUM));
    }

    @Test
    public void groupBy_OnZooCSVWithMinOnSpecies_ShouldHaveExpectedModel(){
        ArrayList<Column> zooModel = new ArrayList<>();

        ColumnString species = new ColumnString("Espèce");
        species.add("Ours");
        species.add("Chien");
        species.add("Oiseau");
        species.add("Porc");
        zooModel.add(species);

        ColumnNumber ages = new ColumnNumber("Âge");
        ages.add(12.0);
        ages.add(6.0);
        ages.add(2.0);
        ages.add(5.0);
        zooModel.add(ages);

        assertEquals(new Dataframe(zooModel), zooDataframe.groupBy("Espèce", Function.MINIMUM));
    }

    @Test
    public void groupBy_OnZooCSVWithMaxOnSpecies_ShouldHaveExpectedModel(){
        ArrayList<Column> zooModel = new ArrayList<>();

        ColumnString species = new ColumnString("Espèce");
        species.add("Ours");
        species.add("Chien");
        species.add("Oiseau");
        species.add("Porc");
        zooModel.add(species);

        ColumnNumber ages = new ColumnNumber("Âge");
        ages.add(14.0);
        ages.add(7.0);
        ages.add(21.0);
        ages.add(5.0);
        zooModel.add(ages);

        assertEquals(new Dataframe(zooModel), zooDataframe.groupBy("Espèce", Function.MAXIMUM));
    }

    @Test
    public void groupBy_WithWrongLabel_ShouldReturnEmptyDataframe(){
        assertTrue(zooDataframe.groupBy("blblblbbl", Function.AVERAGE).isEmpty());
    }

    @Test
    public void aggregate_OnZooCSV_ShouldReturnExpectedValues(){
        ArrayList<Column> zooModel = new ArrayList<>();

        ColumnString functions = new ColumnString(" ");
        functions.add("AVERAGE");
        functions.add("SUM");
        functions.add("MAXIMUM");
        functions.add("MINIMUM");
        zooModel.add(functions);

        ColumnNumber ages = new ColumnNumber("Âge");
        ages.add(9.0);
        ages.add(72.0);
        ages.add(21.0);
        ages.add(2.0);
        zooModel.add(ages);

        assertEquals(new Dataframe(zooModel), zooDataframe.aggregate(new Function[]{Function.AVERAGE, Function.SUM, Function.MAXIMUM, Function.MINIMUM}));
    }

    @Test
    public void aggregate_OnZooCSV_DoesNotChangeOriginalDataframe() throws ColumnHaveDifferentTypesException {
        Dataframe original = new Dataframe("zoo.csv");
        zooDataframe.aggregate(new Function[]{Function.AVERAGE, Function.SUM, Function.MAXIMUM, Function.MINIMUM});
        assertEquals(original, zooDataframe);
    }

    @Test
    public void testEquals_Symmetric() throws ColumnHaveDifferentTypesException {
        Dataframe x = new Dataframe("superZoo.csv");  // equals and hashCode check name field value
        Dataframe y = new Dataframe("superZoo.csv");
        assertTrue(x.equals(y) && y.equals(x));
        assertTrue(x.hashCode() == y.hashCode());
    }
}