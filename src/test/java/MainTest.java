import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MainTest {
    @Test
    public void main_DoesNotThrowException(){
        Main.main(null);
    }

    @Test
    public void testDataframe_WithFileThatExist_ReturnOne(){
        assertEquals(1, Main.testDataframe("superZoo.csv"));
    }

    @Test
    public void testDataframe_WithFileThaDoesNotExist_ReturnZero(){
        assertEquals(0, Main.testDataframe("blblbl"));
    }

    @Test
    public void testDataframe_WithFileThatHasErrors_ReturnZero(){
        assertEquals(0, Main.testDataframe("differentTypes.csv"));
    }
}
