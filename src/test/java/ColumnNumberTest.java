import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ColumnNumberTest {
    private ColumnNumber column;
    private ColumnNumber emptyColumn;
    ArrayList<Number> columnModel;

    @BeforeEach
    public void beforeEach(){
        ArrayList<Number> ages = new ArrayList<>();
        ages.add(42);
        ages.add(-12);
        ages.add(13);
        ages.add(91);
        ages.add(38);
        column = new ColumnNumber("Âge", ages);

        emptyColumn = new ColumnNumber("label");
    }

    @Test
    public void Constructor_WithArrayList_ContainsSameNumbers(){
        assertEquals(42, column.get(0));
        assertEquals(-12, column.get(1));
        assertEquals(13, column.get(2));
        assertEquals(91, column.get(3));
        assertEquals(38, column.get(4));
    }

    @Test
    public void get_GetCorrectIndex_HasCorrectValue(){
        assertEquals(-12, column.get(1));
    }

    @Test
    public void remove_RemoveCorrectIndex_HasDifferentSize(){
        column.remove(1);
        assertEquals(4, column.size());
    }

    @Test
    public void removeDuplicates_WithNoDuplicates_ColumnIsUnchanged(){
        ColumnNumber column2 = column;
        column2.removeDuplicates();
        assertEquals(column, column2);
    }

    @Test
    public void removeDuplicates_WithDuplicates_DuplicatesAreRemoved(){
        ArrayList<Number> agesWithNoDuplicates = new ArrayList<>();
        agesWithNoDuplicates.add(2);
        agesWithNoDuplicates.add(3);
        Column columnWithNoDuplicates = new ColumnNumber("Âge", agesWithNoDuplicates);

        ArrayList<Number> agesWithDuplicates = new ArrayList<>();
        agesWithDuplicates.add(2);
        agesWithDuplicates.add(3);
        agesWithDuplicates.add(2);
        Column columnWithDuplicates = new ColumnNumber("Âge", agesWithDuplicates);
        columnWithDuplicates.removeDuplicates();

        assertEquals(columnWithNoDuplicates, columnWithDuplicates);
    }

    @Test
    public void clear_OnColumn_ShouldBeEmpty(){
        column.clear();
        assertTrue(column.size() == 0);
    }

    @Test
    public void Constructor_OnlyWithLabel_SizeEqualsZero(){
        assertEquals(0, emptyColumn.size());
    }

    @Test
    public void Constructor_OnlyWithLabel_HasCorrectLabel(){
        assertEquals("label", emptyColumn.getLabel());
    }

    @Test
    public void getLabel_NoArguments_ReturnsCorrectLabel(){
        assertEquals("Âge", column.getLabel());
    }

    @Test
    public void size_NoArguments_ReturnsCorrectSize(){
        assertEquals(5, column.size());
    }

    @Test
    public void add_ElementAdded_ElementAddedToTheColumn(){
        int age = 42;
        column.add(age);
        assertEquals(age, column.get(column.size()-1));
    }

    @Test
    public void sum_OnEmptyArray_ReturnsZero(){
        assertEquals(Double.NaN, emptyColumn.sum());
    }

    @Test
    public void sum_OnNumbers_ReturnsCorrectSum(){
        assertEquals(172, column.sum());
    }

    @Test
    public void average_OnEmptyArray_ReturnsZero(){
        assertEquals(Double.NaN, emptyColumn.average());
    }

    @Test
    public void average_OnNumbers_ReturnsCorrectAverage(){ assertEquals(34.4, column.average());
    }

    @Test
    public void minimum_OnEmptyArray_ReturnsZero(){
        assertEquals(Double.NaN, emptyColumn.minimum());
    }

    @Test
    public void minimum_OnNumbers_ReturnsCorrectMinimum(){ assertEquals(-12, column.minimum());
    }

    @Test
    public void maximum_OnEmptyArray_ReturnsZero(){
        assertEquals(Double.NaN, emptyColumn.maximum());
    }

    @Test
    public void maximum_OnNumbers_ReturnsCorrectMaximum(){ assertEquals(91, column.maximum()); }

    @Test
    public void testEquals_Symmetric(){
        ColumnNumber x = new ColumnNumber("test", columnModel);  // equals and hashCode check name field value
        ColumnNumber y = new ColumnNumber("test", columnModel);
        assertTrue(x.equals(y) && y.equals(x));
        assertTrue(x.hashCode() == y.hashCode());
    }

}