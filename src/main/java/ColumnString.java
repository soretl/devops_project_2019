import java.util.ArrayList;

public class ColumnString extends Column<String>{

    public ColumnString(String label) {
        super(label);
    }

    public ColumnString(String label, ArrayList<String> column) {
        super(label, column);
    }
}
