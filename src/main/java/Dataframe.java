import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;

import Exceptions.ColumnHaveDifferentTypesException;
import Exceptions.WrongOperationOnColumnException;
import com.opencsv.CSVReader;
import org.apache.commons.lang3.StringUtils;

import java.io.Reader;

/**
 * An implementation of the Dataframe from Python's library Panda.
 */
public class Dataframe<T> {
    private ArrayList<Column> columns;

    /**
     * Construct the columns with an array list.
     * Mostly used for tests.
     * @param column the ArrayList of columns used to fill the columns.
     */
    public Dataframe(ArrayList<Column> column){
        this.columns = column;
    }

    /**
     * Construct the columns by parsing a csv file.
     * @param pathToCSV the path to the csv file to parse
     */
    public Dataframe(String pathToCSV) throws ColumnHaveDifferentTypesException {
        columns = new ArrayList<>();
        try (
                Reader reader = new BufferedReader(new InputStreamReader(
                        this.getClass().getResourceAsStream("/csv/" + pathToCSV)));
                CSVReader csvReader = new CSVReader(reader)
        ) {
            // Reading Records One by One in a String array
            String[] nextRecord;
            /* Creating each column with the right type
            We need to read the first two lines :
                the first line to know the label
                the second line to know the type of the column (number or string)
            */
            String[] labels = csvReader.readNext();
            String[] firstLine = csvReader.readNext();

            if (!(labels == null) && !(firstLine == null)) {
                for (int i = 0; i < firstLine.length; i++) {
                    if (StringUtils.isNumeric(firstLine[i])) {
                        Column<Number> column = new ColumnNumber(labels[i]);
                        column.add(NumberFormat.getInstance().parse(firstLine[i]));
                        columns.add(column);
                    } else {
                        Column<String> column = new ColumnString(labels[i]);
                        column.add(firstLine[i]);
                        columns.add(column);
                    }
                }

                // Reading the remaining lines
                while ((nextRecord = csvReader.readNext()) != null) {
                    for (int columnNumber = 0; columnNumber < nextRecord.length; columnNumber++) {
                        if (StringUtils.isNumeric(nextRecord[columnNumber])) {
                            Number number = NumberFormat.getInstance().parse(nextRecord[columnNumber]);
                            if(get(columnNumber) instanceof ColumnNumber){
                                get(columnNumber).add(number);
                            }else{
                                throw new ColumnHaveDifferentTypesException();
                            }
                        } else {
                            get(columnNumber).add(nextRecord[columnNumber]);
                        }
                    }
                }
            }
        }catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get item from object for given key (DataFrame column, Panel slice, etc.).
     * @param columnNumber DataFrame column number
     * @return same type as items contained in object
     */
    public Column get(int columnNumber){
        return columns.get(columnNumber);
    }

    /**
     * Return the size of the dataframe
     * The size of a dataframe is the number of columns it has.
     * @return the size of the dataframe.
     */
    public int size(){
        return columns.size();
    }

    /**
     * Check if the dataframe is empty.
     * A dataframe is considered empty if it has no content.
     * A dataframe with only labels is considered empty.
     * @return true if the dataframe is empty
     */
    public boolean isEmpty(){
        if(size() != 0){
            for(int i = 0; i < size(); i++){
                if(this.get(i).size() > 0){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Return the height of the dataframe, i.e, the length of it's longest column
     * @return the height of the dataframe
     */
    public int getHeight(){
        int width = 0;
        for(int i = 0; i < columns.size(); i++){
            int columnSize = columns.get(i).size();

            if(columnSize > width){
                width = columnSize;
            }
        }

        return width;
    }

    @Override
    public String toString() {
        return "Dataframe{" +
                "columns=" + columns.toString() +
                '}';
    }

    /**
     * Print a line of the dataframe
     * @param line the index of the line to print
     * @return 1 if it worked,0 if there was a problem
     *          i.e, the line number didn't exist
     */
    public int printLine(int line){
        if(line > getHeight() || line < 0) {
            return 0;
        }

        for(int i = 0; i < columns.size(); i++){
            System.out.print("[" + columns.get(i).get(line) +"]");
        }
        System.out.println();
        return 1;
    }

    /**
     * Print the labels of each column
     */
    public void printLabels(){
        for(int i = 0; i < columns.size(); i++){
            System.out.print("[" + columns.get(i).getLabel() +"]");
        }
        System.out.println();
    }


    /**
     * Create a dataframe by selecting lines
     * @param indices the array of indices to select
     * @return the selected dataframe
     */
    public Dataframe iloc(int[] indices){
        ArrayList<Column> newColumns = new ArrayList<>();

        for(int i = 0; i < columns.size(); i++){
            Column columnToCopy = this.get(i);
            Column newColumn;

            if(this.get(i) instanceof ColumnNumber){
                newColumn = new ColumnNumber(columnToCopy.getLabel());
            }else{
                newColumn = new ColumnString(columnToCopy.getLabel());
            }

            newColumns.add(newColumn);

            for(int j = 0; j < indices.length; j++){
                if(indices[j] < getHeight() && indices[j] >= 0) {
                    newColumn.add(columnToCopy.get(indices[j]));
                }
            }
        }

        return new Dataframe(newColumns);
    }

    /**
     * Create a new dataframe with a line selection.
     * @param lineIndex the line to select
     * @return the selected dataframe
     */
    public Dataframe iloc(int lineIndex){
        int[] indices = new int[]{lineIndex};
        return iloc(indices);
    }

    /**
     * Create a dataframe by selecting a range of lines.
     * Note : start and stop are included in the indices
     * @param start the index to start the selection
     * @param stop the index to stop the selection
     * @return the selected dataframe
     */
    public Dataframe iloc(int start, int stop){
        if(stop > start && start >= 0){
            int[] indices = new int[(stop-start)+1];

            for(int i = start; i <= stop; i++){
                indices[i] = i;
            }

            return iloc(indices);
        }
        // to return an empty dataframe
        return iloc(-1);
    }

    /**
     * Create a dataframe by selecting several columns
     * @param labels the array of labels
     * @return the selected dataframe
     */
    public Dataframe loc(String[] labels){
        ArrayList<Column> newColumns = new ArrayList<>();

        for(int i = 0; i < columns.size(); i++){
            Column newColumn = columns.get(i);
            for(int j = 0; j < labels.length; j++){
                if(newColumn.getLabel().equals(labels[j])){
                    newColumns.add(newColumn);
                }
            }
        }

        return new Dataframe(newColumns);
    }

    /**
     * Create a dataframe by selecting a column
     * @param label the label to select
     * @return the selected dataframe
     */
    public Dataframe loc(String label){
        String[] labels = new String[]{label};
        return loc(labels);
    }

    /**
     * Create a dataframe by selecting a range of columns
     * @param start the first label to select
     * @param stop the last label to select
     * @return the selected dataframe
     */
    public Dataframe loc(String start, String stop){
        Column column = columns.get(0);

        // get the index of the start label
        int startIndex = 0;
        while(!column.getLabel().equals(start) && startIndex < size()){
            column = columns.get(startIndex);
            startIndex++;
        }

        // get the index of the stop label
        int stopIndex = startIndex;
        while(!column.getLabel().equals("stop") && stopIndex < size()){
            column = columns.get(stopIndex);
            stopIndex++;
        }

        if(stopIndex > size()){
            return null; // to return an empty dataframe
        }

        // Filling the labels
        String[] labels = new String[(stopIndex-startIndex)+1];
        for(int i = startIndex; i < stopIndex; i++){
            labels[i] = get(i).getLabel();
        }

        return loc(labels);
    }

    /**
     * Return the index of the column of a specified label
     * @param label the label of the column
     * @return the index of the column, -1 if not found
     */
    private int getIndexOfLabel(String label){
        for(int i = 0; i < size(); i++){
            if(columns.get(i).getLabel().equals(label)){
                return i;
            }
        }
        return -1;
    }

    /**
     * Create a copy of column
     * dirty af ¯\_(ツ)_/¯
     */
    private Column copyColumn(Column column){
        if(column instanceof ColumnNumber){
            return new ColumnNumber(new String(column.getLabel()), new ArrayList<Number>(column.getContent()));
        }else{
            return new ColumnString(new String(column.getLabel()), new ArrayList<String>(column.getContent()));
        }
    }

    /**
     * Create a new dataframe by grouping on a column.
     * Regroup similar datas from the same columns.
     * Apply a function on all the others, if the function cannot be applied, ignore these columns.
     * @param label the label of the column
     * @param function the function to apply on other columns
     * @return the grouped dataframe, an empty dataframe if nothing found.
     */
    public Dataframe groupBy(String label, Function function) {
        // Creation of the new dataframe
        ArrayList<Column> newColumns = new ArrayList<>();

        // Removing duplicates of the column to group by
        int indexOfLabel = getIndexOfLabel(label);
        if(indexOfLabel == -1){
            return iloc(-1);
        }

        Column columnToGroupBy = copyColumn(columns.get(indexOfLabel));
        columnToGroupBy.removeDuplicates();
        newColumns.add(columnToGroupBy);

        // Splitting other columns
        for(int i = 0; i < size(); i++) { // Iterating on every column
            if(i != indexOfLabel && columns.get(i) instanceof ColumnNumber) { // Ignoring the grouped by column
                Column currentColumn = columns.get(i);
                ColumnNumber newColumn = new ColumnNumber(currentColumn.getLabel());

                for (int j = 0; j < columnToGroupBy.size(); j++) { // Iterating on column to group by
                    Object value = columnToGroupBy.get(j);
                    Column smallerColumnToCompute = new ColumnNumber(currentColumn.getLabel());

                    for (int k = 0; k < currentColumn.size(); k++) { // Iterating on lines of a column
                        if (columns.get(indexOfLabel).get(k).equals(value)) {
                            smallerColumnToCompute.add(currentColumn.get(k));
                        }
                    }

                    try {
                        Double result = smallerColumnToCompute.applyFunctionOnColumn(function);
                        newColumn.add(result);
                    } catch (WrongOperationOnColumnException ignored) { }
                }
                newColumns.add(newColumn);
            }
        }

        return new Dataframe(newColumns);
    }

    /**
     * Call function on each column
     * Create a line to show the result of each function
     * @param functions the functions to call
     * @return the new dataframe
     */
    public Dataframe aggregate(Function[] functions){
        // Global structure of the new dataframe
        ArrayList<Column> newColumns= new ArrayList<>();
        Column functionColumn = new ColumnString(" ");
        newColumns.add(functionColumn);

        for(int j = 0; j < columns.size(); j++){
            Column columnCopy = copyColumn(columns.get(j));
            columnCopy.clear();
            newColumns.add(columnCopy);
        }

        // Filling the new dataframe
        for(int i = 0; i < functions.length; i++){
            functionColumn.add(functions[i].toString());

            for(int j = 0; j < columns.size(); j++){
                    Column columnCopy = copyColumn(columns.get(j));
                    try {
                        double resultat = columnCopy.applyFunctionOnColumn(functions[i]);
                        newColumns.get(j+1).add(resultat);
                    }catch (WrongOperationOnColumnException ignored) {
                        newColumns.get(j+1).add(" ");
                    }

            }
        }

        // Removing the string columns
        Iterator it = newColumns.iterator();
        it.next();
        while(it.hasNext()){
            if(it.next() instanceof ColumnString){
                it.remove();
            }
        }

        return new Dataframe(newColumns);
    }

    /**
     * Print the first lines
     * @param linesNumber the number of lines to print
     */
    public void printFirstLines(int linesNumber){
        printLabels();
        if(linesNumber > getHeight()){
            linesNumber = getHeight();
        }

        for(int i = 0; i < linesNumber; i++){
            printLine(i);
        }
    }

    /**
     * Print the last lines
     * @param linesNumber the number of lines to print
     */
    public void printLastLines(int linesNumber){
        printLabels();
        for(int i = getHeight()-linesNumber; i < getHeight(); i++){
            printLine(i);
        }
    }

    /**
     * Print the data frame, line by line
     */
    public void print(){
        printLabels();
        for(int i = 0; i < getHeight(); i++){
            printLine(i);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dataframe dataframe = (Dataframe) o;
        return Objects.equals(columns, dataframe.columns);
    }

    @Override
    public int hashCode() {
        return Objects.hash(columns);
    }
}
