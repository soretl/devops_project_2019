package Exceptions;

/**
 * Is thrown when a column have two values of a different type.
 * Note : Is not a thrown if the column is made of strings : the values are converted.
 */
public class ColumnHaveDifferentTypesException extends Exception {
    public ColumnHaveDifferentTypesException() {
    }
}
