package Exceptions;

/**
 * Exception called when an operation is used on a column of the wrong type.
 * For instance, asking for the average of a string column.
 */
public class WrongOperationOnColumnException extends Exception{
    public WrongOperationOnColumnException() {
    }
}
