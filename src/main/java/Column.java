import Exceptions.WrongOperationOnColumnException;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A content of a dataframe.
 * @param <T> generic type filling the content
 */
public abstract class Column<T> {
    private String label;
    private ArrayList<T> content;

    /**
     * Create an empty content
     * @param label name of the content
     */
    public Column(String label) {
        this.label = label;
        this.content = new ArrayList<>();
    }

    /**
     * Create a content from data
     * @param label name of the content
     * @param content raw data
     */
    public Column(String label,ArrayList<T> content) {
        this.label = label;
        this.content = content;
    }

    /**
     * Get the column's content.
     * @return the content of the column
     */
    public ArrayList<T> getContent() {
        return content;
    }

    /**
     * Return the element at the specified index.
     * @param index index of element
     * @return the element at the specified index
     */
    public T get(int index){
        return content.get(index);
    }

    /**
     * Remove an element from the column
     * @param index the index of the element to remove
     */
    public void remove(int index){
        content.remove(index);
    }

    public void removeDuplicates(){
        ArrayList<T> newList = new ArrayList<T>();
        for (T element : content) {
            if (!newList.contains(element)) {
                newList.add(element);
            }
        }

        this.content = newList;
    }

    /**
     * Remove all content of the column
     */
    public void clear(){
        content.clear();
    }

    /**
     * Return the size of the content.
     * @return the number of elements of the content.
     */
    public int size(){
        return content.size();
    }

    /**
     * Return the label of the content.
     * @return label of the content
     */
    public String getLabel() {
        return label;
    }

    /**
     * Add an element to the content
     * @param element the element to add
     */
    public void add(T element){
        content.add(element);
    }

    /**
     * Compute the sum of the content
     * @return the sum of the content, NaN on empty column
     */
    public Double sum() throws WrongOperationOnColumnException {
        throw new WrongOperationOnColumnException();
    }

    /**
     * Compute the average of the content
     * @return the average of the content, NaN on empty column
     */
    public Double average () throws WrongOperationOnColumnException {
        throw new WrongOperationOnColumnException();
    }

    /**
     * Compute the minimum of the content
     * @return the minimum of the content, NaN on empty column
     */
    public Double minimum () throws WrongOperationOnColumnException {
        throw new WrongOperationOnColumnException();
    }

    /**
     * Compute the maximum of the content
     * @return the maximum of the content, NaN on empty column
     */
    public Double maximum () throws WrongOperationOnColumnException {
        throw new WrongOperationOnColumnException();
    }

    /**
     * Apply a function given with the Function enum to the column
     * @param function the function to apply
     * @return the result of the applied function
     */
    public Double applyFunctionOnColumn( Function function) throws WrongOperationOnColumnException {
        switch(function){
            case SUM:
                return sum();
            case AVERAGE:
                return average();
            case MAXIMUM:
                return maximum();
            case MINIMUM:
                return minimum();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Column{" +
                "label='" + label + '\'' +
                ", content=" + content.toString() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Column<?> column = (Column<?>) o;
        return Objects.equals(label, column.label) &&
                Objects.equals(content, column.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(label, content);
    }
}
