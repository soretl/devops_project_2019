import Exceptions.ColumnHaveDifferentTypesException;

public class Main {
    /**
     * Simple test of a dataframe
     * @param pathToCSV the path to the csv file to test
     * @return 1 if everything went well, 0 otherwise
     */
    public static int testDataframe(String pathToCSV){
        Dataframe dataframe = null;
        try {
            System.out.println("Affichage du dataframe parsé zoo.csv");
            dataframe = new Dataframe(pathToCSV);
            dataframe.print();

            System.out.println("\nSélection de dataframe par lignes");
            dataframe.iloc(0, 2).print();

            System.out.println("\nSélection de dataframe par colonnes");
            dataframe.loc("Nom").print();

            System.out.println("\nGroup by sur l'espèce et moyenne");
            dataframe.groupBy("Espèce", Function.AVERAGE).print();

            System.out.println("\nAggregate");
            dataframe.aggregate(new Function[]{Function.AVERAGE, Function.SUM, Function.MAXIMUM, Function.MINIMUM}).print();
            return 1;
        } catch (ColumnHaveDifferentTypesException | NullPointerException e) {
            System.out.println("Les valeurs d'une colonne doivent être de même type.");
            return 0;
        }
    }

    public static void main(String[] args) {
        testDataframe("superZoo.csv");
    }
}
