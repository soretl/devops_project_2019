import java.util.ArrayList;

public class ColumnNumber extends Column<Number>{
    public ColumnNumber(String label) {
        super(label);
    }

    public ColumnNumber(String label, ArrayList<Number> column) {
        super(label, column);
    }

    @Override
    public Double sum() {
        if(this.getContent().isEmpty()){
            return Double.NaN;
        }

        Double sum = 0.0;
        if(!this.getContent().isEmpty()){
            for(int i = 0; i < this.getContent().size(); i++){
                sum += this.getContent().get(i).doubleValue();
            }
        }
        return sum;
    }

    @Override
    public Double average() {
        return sum() / size();
    }

    @Override
    public Double minimum() {
        if(!this.getContent().isEmpty()){
            Double minimum = this.getContent().get(0).doubleValue();
            for(int i = 0; i < this.getContent().size(); i++){
                Double element = this.getContent().get(i).doubleValue();
                if(element < minimum){
                    minimum = element;
                }
            }
            return minimum;
        }
        return Double.NaN;
    }

    @Override
    public Double maximum() {
        if(!this.getContent().isEmpty()){
            Double maximum = this.getContent().get(0).doubleValue();
            for(int i = 0; i < this.getContent().size(); i++){
                Double element = this.getContent().get(i).doubleValue();
                if(element > maximum){
                    maximum = element;
                }
            }
            return maximum;
        }
        return Double.NaN;
    }
}
