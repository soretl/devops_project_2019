# JPanda
### Une implémentation de la librairie panda en Java

[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/soretl/devops_project_2019/badges/master/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/soretl/devops_project_2019/commits/master)
[![coverage report](https://gricad-gitlab.univ-grenoble-alpes.fr/soretl/devops_project_2019/badges/master/coverage.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/soretl/devops_project_2019/commits/master)


## Fonctionnalités
* Création de dataframe à partir d'un fichier CSV ou d'un tableau de colonnes.
* Affichage de dataframes.
    * Affichage d'une ligne spécifique.
    * Affichage des x premières/dernières lignes.
    * Affichage du dataframe en entier.
* Sélection dans les dataframes.
    * Sélection par colonne (loc) avec plusieurs manières de sélectionner (par label, avec labels multiples, d'un label de départ à un label d'arrêt).
    * Sélection par ligne (iloc) avec plusieurs manières de sélectionner (par numéro de ligne, avec plusieurs numéros de lignes, d'une ligne de départ à une ligne d'arrêt).
* Statistiques sur une colonne de dataframe (Min, Max, Moyenne, Somme)
* Group By.
    * Regroupe les éléments d'une colonne choisie, désignée par un label.
    * Applique une fonction sur les éléments des autres colonnes du dataframe.
* Aggregate.
    * Applique une liste de fonctions choisies sur les sous-groupes.

## Installation

### Docker
Une image docker est disponible sur [docker hub](https://hub.docker.com/r/croustimag/jpanda) pour récupérer l'image (en super utilisateur):

```
docker pull croustimag/jpanda
```

puis pour lancer (toujours en super utilisateur):

```
docker run croustimag/jpanda
```

Il est possible de récupérer la documentation depuis l'image docker:
```
CID=$(docker create <croustimag/jpanda>)
docker cp ${CID}:app/DevOps_Project_2019-1.0-SNAPSHOT-javadoc.jar <destination> 
docker rm ${CID}
```

### Sources
Il est aussi possible de cloner ce repo puis de compiler grâce à:

```
mvn compile
```
(la documentation se trouvera dans target/site/apidoc)
ou d'exporter en jar:

```
mvn package
```

et même de lancer les tests unitaires:

```
mvn tests
```
(le rapport de couverture du code se trouvera dans target/site/jacoco)

## Utilisation

Le main est un exemple d'utilisation.
Il commence par charger un fichier csv contenant des données sur des animaux.
Il fait ensuite quelques sélections par ligne et colonnes.
Puis, il réalise un group by sur l'espèce de l'animal et y associe la moyenne des autres colonnes (ici l'âge de l'animal).
Finalement, il fait une aggrégation sur le dataframe et calcule les valeurs de toutes les fonctions traitées sur les colonnes.

## Outils utilisés

Cette librairie est développée en Java 8 avec Maven. L'intégration continue est gérée grâce au GitLab CI. Le déploiment continue est géré par GitLab CD et déploie une image docker sur dockerhub (voir la rubrique utilisation). Les tests sont fait en JUnit 5 et mis en place grâce au plugin surefire de Maven. La couverture de code est assurée par le plugin jacoco de maven, le rapport de couverture de code est ensuite analysé par le GitLab CI pour créer le badge de couverture de code au
haut de cette page. Un minimum de 95% de couverture de code est requis pour que la release soit acceptée. La création de la javadoc est aussi gérée par Maven et integrée dans l'image docker de la release par le GitLab CD. 
